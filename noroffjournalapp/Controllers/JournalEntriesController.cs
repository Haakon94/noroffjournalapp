﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using noroffjournalapp.Data;
using noroffjournalapp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Security;
using Microsoft.AspNetCore.Authorization;
using System.Diagnostics;

namespace noroffjournalapp.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class JournalEntriesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public JournalEntriesController(ApplicationDbContext context, UserManager<ApplicationUser> userManager) {
            _userManager = userManager;
            _context = context;
        }

        [HttpGet("{id}")]
        public ActionResult<JournalEntry> GetJournalEntryByID(int id) {
            var journalEntry = _context.JournalEntry.Find(id);

            return journalEntry;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<JournalEntry>>> GetAllJournalEntries() {
            var user = await _userManager.FindByIdAsync(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            Debug.WriteLine("#########################         " + HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            Debug.WriteLine("#########################         " + user);

            var journalEntries = _context.JournalEntry.Where(entry => entry.ApplicationUser == user).OrderByDescending(s => s.Id).ToList();
            return Ok(journalEntries);

        }

        [HttpPost]
        public async Task<ActionResult<JournalEntry>> AddJournalEntry(JournalEntry journalentry)
        {
            var user = await _userManager.FindByIdAsync(HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            journalentry.ApplicationUser = user;
            _context.JournalEntry.Add(journalentry);
            _context.SaveChanges();

            return CreatedAtAction("GetJournalEntryByID", new JournalEntry { Id = journalentry.Id }, journalentry);
        }

        [HttpPut("{id}")]
        public ActionResult UpdateAJournalEntry(int id, JournalEntry journalEntry) {
            if(id != journalEntry.Id) {
                return BadRequest();
            }

            _context.Entry(journalEntry).State = EntityState.Modified;
            _context.SaveChanges();

            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult<JournalEntry> DeleteJournalEntry(int id) {
            var journalEntry = _context.JournalEntry.Find(id);

            if(journalEntry == null) {
                return NotFound();
            }

            _context.JournalEntry.Remove(journalEntry);
            _context.SaveChanges();

            return journalEntry;
        }

    }
}