﻿import React, { Component } from 'react';

class JournalEntryForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            title: '', subtitle: '', body: '', imgUrl: ''
        };

        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    handleOnClick() {
        if (this.props.onChange) {
            this.props.onChange(this.state);
        }
    }

    render() {
        return (
            <div className="form-container">
                
                <div className="row form-row">
                    <h5>Add a new journal entry</h5>
                    <input className="form-control" type="text" placeholder="Title" name="title" onChange={this.handleChange} />
                    <input className="form-control" type="text" placeholder="Subtitle" name="subtitle" onChange={this.handleChange} />
                    <input className="form-control" type="text" placeholder="Add your image url" name="imgUrl" onChange={this.handleChange} />
                    <textarea className="form-control" rows="3" placeholder="Write your journal entry here..." name="body" onChange={this.handleChange}></textarea>                    
                    <button className="btn btn-primary" onClick={() => this.props.onClick(this.state)} style={{ 'marginTop': "1em" }}>Create</button>
                </div>

            </div>
        );
    }
}
export default JournalEntryForm;