import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import authService from '../api-authorization/AuthorizeService';

class JournalEntryCard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            updateForm: false, 
            deleteForm: false,
            id: props.id, title: props.title, subtitle: props.subtitle, body: props.body, imgUrl: props.imgUrl
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.handleDelete = this.handleDelete.bind(this);

      
    }

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value })
        
    }

    async handleUpdate() {
        this.toggleModalForm();
        
        var postData;
        if (!this.state.imgUrl || this.state.imgUrl.match(/\.(jpeg|jpg|gif|png)$/) == null) {
            postData = {
                id: this.props.id,
                title: this.state.title,
                subtitle: this.state.subtitle,
                body: this.state.body,
                imgUrl: 'https://www.imgworlds.com/wp-content/uploads/2015/12/18-CONTACTUS-HEADER.jpg',
            };
        } else {
            postData = {
                id: this.props.id,
                title: this.state.title,
                subtitle: this.state.subtitle,
                body: this.state.body,
                imgUrl: this.state.imgUrl,
            };
        }
        const token = await authService.getAccessToken();
        console.log(this.props.id);
        fetch('api/journalentries/' + this.props.id, {
            headers: !token ? {} : {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json',
                'Accept': 'application/json'

            },
            method: 'PUT',
            body: JSON.stringify(postData),
        }).then((responseJson) => {
                this.props.refresh();
            })
    }

    async handleDelete() {
        const token = await authService.getAccessToken();
        fetch('api/journalentries/' + this.props.id, {
            headers: !token ? {} : {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            method: 'DELETE',
        }).then((responseJson) => {
                this.props.refresh();
            })
    }

    toggleModalForm = () => {
        this.setState(prevState => ({
            updateForm: !prevState.updateForm
        }))
    }

    render() {
        return (
            <div>
                <div className="card mb-2" style={{ 'marginTop': "1em" }}>
                    <div className="row no-gutters journal-card">
                        <div className="col-4 align-self-center">
                            <img id="entry-img" src={this.props.imgUrl} alt="" className="card-img" />
                        </div>
                        <div className="col-6">
                            <div className="card-body">
                                <h5 id="entry-title" className="card-title">{this.props.title}</h5>
                                <h6 id="entry-subtitle" style={{ 'fontStyle': 'italic' }}>{this.props.subtitle}</h6>
                                <p id="entry-body" className="card-text">{this.props.body}</p>
                                <p className="card-text"><small id="entry-published" className="text-muted"> </small></p>
                                <p className="card-text"><small id="entry-updated" className="text-muted"></small></p>
                                <button className="btn btn-primary" type="button" onClick={this.toggleModalForm}>
                                    Update
                                </button>
                                <button className="btn btn-danger" type="button" onClick={this.handleDelete}>
                                    Delete
                                </button>
                        </div>
                        </div>

                        <Modal isOpen={this.state.updateForm} toggle={this.toggleModalForm}>
                            <ModalHeader toggle={this.toggleModalForm}>Update the journal entry</ModalHeader>
                            <ModalBody>
                                <div className="row update-form">
                                    <input className="form-control" type="text" defaultValue={this.props.title} placeholder="Title" name="title" onChange={this.handleChange} />
                                    <input className="form-control" type="text" defaultValue={this.props.subtitle} placeholder="Subtitle" name="subtitle" onChange={this.handleChange} />
                                    <input className="form-control" type="text" defaultValue={this.props.imgUrl} placeholder="Add your image url" name="imgUrl" onChange={this.handleChange} />
                                    <textarea className="form-control" rows="3" defaultValue={this.props.body} placeholder="Write your journal entry here..." name="body" onChange={this.handleChange}></textarea>
                                    <button className="btn btn-primary" onClick={this.handleUpdate} style={{ 'marginTop': "1em" }}>Update</button>
                                </div>
                            </ModalBody>
                        </Modal>
                </div>
            </div>
        </div>
        );
    }
}

export default JournalEntryCard;
