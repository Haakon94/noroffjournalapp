import React, { Component } from 'react';
import authService from './api-authorization/AuthorizeService'
import JournalEntryForm from './JournalEntryForm';
import JournalEntryCard from './JournalEntryCard/JournalEntryCard';

export class FetchData extends Component {
    static displayName = FetchData.name;

    constructor(props) {
        super(props);
        this.state = {
            entries: [],
            loading: true,
            selectedEntry: {}
        };

        this.populateJournalCards = this.populateJournalCards.bind(this);
    }

    componentDidMount() {
        this.populateJournalCards();
    }

    renderEntriesTable(entries) {
        return (
            <div>
                <div className="journal-cards">
                    {entries.map(entry =>
                        <JournalEntryCard key={entry.id} {...entry} refresh={this.populateJournalCards} />

                    )}              
                    
                </div>  
            </div>
        );
    }

  render() {
    let contents = this.state.loading
      ? <p><em>loading...</em></p>
        : this.renderEntriesTable(this.state.entries);

    return (
        <div>
            <JournalEntryForm onClick={data => {
                this.handleCreate(data);
            }} />
            {contents}
        </div>
    );
    }


     async handleCreate(data) {
         var postData;
         if (!data.imgUrl || data.imgUrl.match(/\.(jpeg|jpg|gif|png)$/) == null) {
            postData = {
                title: data.title,
                subtitle: data.subtitle,
                body: data.body,
                imgUrl: 'https://www.imgworlds.com/wp-content/uploads/2015/12/18-CONTACTUS-HEADER.jpg',
            };
        } else {
        postData = {
            title: data.title,
            subtitle: data.subtitle,
            body: data.body,
            imgUrl: data.imgUrl,
            };
        }
        const token = await authService.getAccessToken();

        fetch('api/journalentries', {
            headers: !token ? {} : {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json',
                'Accept': 'application/json'

            },
            method: 'POST',
            body: JSON.stringify(postData),
        }).then((response) => response.json())
            .then((responseJson) => {
                this.populateJournalCards();
            })
    }

    async populateJournalCards() {
        const token = await authService.getAccessToken();
        const response = await fetch('api/journalentries', {
            headers: !token ? {} : { 'Authorization': `Bearer ${token}` }
        });
        const data = await response.json();
        this.setState({ entries: data, loading: false });
    }
}
