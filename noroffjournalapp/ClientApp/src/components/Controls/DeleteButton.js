import React, { Component } from 'react';

const DeleteButton = props => {
    const { deleteButtonOnClick, deleteButtonTarget, entry } = props;

    const deleteButton = $(`
		<button 
			type="button" 
            class="btn btn-outline-danger"
            data-toggle="modal" 
			data-target="${deleteButtonTarget}">
				<i class="fas fa-trash-alt"></i>
		</button>`);

    deleteButton.on("click", () => deleteButtonOnClick(entry));

    return deleteButton;
};

export default DeleteButton;