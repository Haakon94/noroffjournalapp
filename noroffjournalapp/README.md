﻿# JOURNAL APPLICATION
_________________________________________

***Project by Kenneth, Haakonand Sarah***
_________________________________________

###### You can... 

- ...create a new user with email and password

- ...login if you have an existing user

- ...see all previously made entries

- ...create new journal entries to your journal when you are logged in 

- ...add img-url's to your entries

- ...update and delete entries for your user

- ...logout if you are logged in

_________________________________________

###### The app...

- ...is deployed to Azure, but does not have a connected database there.

- ...has a database with users and entries

- ...checks the database if the email is unique when signing up

- ...hashes your password before adding it to the db

- ...validates login credentials

- ...shows only the journal entries for the logged in user 

- ...logs you out after 1 hour of inactivity

- ...works fairly good on phones
