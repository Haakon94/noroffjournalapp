﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace noroffjournalapp.Models {
    public class JournalEntry {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Subtitle { get; set; }

        public string ImgUrl { get; set; }

        public string Body { get; set; }

        public ApplicationUser ApplicationUser { get; set; }
    }
}
